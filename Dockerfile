FROM node:8-alpine

WORKDIR /app

RUN npm i -g  pm2

RUN npm install

EXPOSE 3000

CMD ["pm2-docker", "server.js"]

