const getColors = require('get-image-colors')


app.get('color/hex/:artist/:album', (req, res) => {
  let artist = req.params.artist
  let album = req.params.album
  let imageURL = `${artist}_${album}.jpg`
  getColors(`../snare/static/albums/${imageURL}`).then(colors => {
  // getColors(`../snare/static/albums/${imageURL.replace(/%20/g, " ")}`).then(colors => {
    res.send(colors.map(color => color.hex()))
  })
})
