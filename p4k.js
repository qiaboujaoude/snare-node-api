pitchfork = require('pitchfork')

module.exports = {
  getAlbumReview: (album, artist) => {
    return new Promise((resolve, reject) => {
      const list = []
      const search = new pitchfork.Search(`${album} ${artist}`)
      search.promise
        .then(results => {
          results.forEach(review => {
            list.push(review.attributes)
          })
          resolve(list)
        })
        .catch(err => console.log(err))
    })
  }
}

