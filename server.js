const express   = require('express'),
      cors      = require('cors'),
      p4k       = require('./p4k')

const app = express()
app.use(cors())

app.get('/pitchfork/:album/:artist', (req, res) => {
  p4k.getAlbumReview(req.params.album, req.params.artist)
    .then(r => res.send(r))
    .catch(err => console.log(err))
})

app.listen(3000)
